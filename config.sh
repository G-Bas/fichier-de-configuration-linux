#!/bin/bash

# Ce script met les fichiers texte à leurs emplacements respectifs
# et permet de les convertir en fichiers de configuration

# Ajout de redshift.conf seulement pour XFCE ou Cinnamon
if [ "$XDG_CURRENT_DESKTOP" = "XFCE" ] | [ "$XDG_CURRENT_DESKTOP" = "X-Cinnamon" ]; then
    cp redshift.conf.txt ~/.config/redshift/redshift.conf
    echo "La copie de redshift.conf est faite."
else
    echo "La copie de redshift.conf n'est pas faite."
fi

# Couleurs personnalisées de ls
cp ls_colors.txt ~/.ls_colors
dircolors -p > ~/.ls_colors

# Modification du swappiness
sudo bash -c 'echo "vm.swappiness=10" >> /etc/sysctl.conf'
sudo sysctl -p