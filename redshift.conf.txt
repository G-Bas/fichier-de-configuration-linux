; Paramètres globaux pour redshift.
[redshift]

; Réglage des températures de jour et nuit  de l'écran
temp-day=6000
temp-night=1700

; Transition entre le jour et la nuit
transition=1

; Fondu progressif de passage de température
fade=1

; Réglage de la luminosité jour / nuit
brightness-day=1.0
brightness-night=0.7

; Gamma de l'écran
gamma=0.8

; Location de l'utilisateur
location-provide=manual

[manual]

lat=45.7014
lon=-0.3818

; Méthode d'ajustement randr

[randr]

screen=0
